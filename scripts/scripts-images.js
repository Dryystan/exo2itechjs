function rotate() {
  var images = document.querySelectorAll("img");
  var images_array = Array.prototype.slice.call(images, 0);

  images_array.sort(function(a, b) {
    if (a.alt > b.alt) return 1;
    if (a.alt < b.alt) return -1;
    return 0;
  })
  console.log(images_array);

  let image_debut_src = images_array[0].src;
  let image_debut_alt = images_array[0].alt;
  for(let i = 0; i < images_array.length; i++) {
    if( i != images_array.length - 1) {
      images_array[i].src = images_array[i+1].src;
      images_array[i].alt = images_array[i+1].alt;
    }
    else {
      images_array[i].src = image_debut_src;
      images_array[i].alt = image_debut_alt;
    }
  }
}
