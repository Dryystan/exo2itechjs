var columns = document.querySelectorAll("#morpion-grid .col-3");
var game_message = document.querySelector("#game-message");
// Stockage de la grille côté JS pour faciliter le suivi d'avancement de partie
var grille = [["-", "-", "-"], ["-", "-", "-"], ["-", "-", "-"]];
var player_one = true;
var gagne = false;

columns.forEach(c => {
  c.addEventListener("click", function() {
    morpion(this);
  });
});

function morpion(col) {
  // Si la grille
  if(grille[Math.floor(col.dataset.num / 3)][col.dataset.num % 3] !== "-" || gagne) {
    return;
  }

  // Remplissage de la grille par l'un des deux joueurs
  if(player_one) {
    col.getElementsByTagName("p").item(0).innerHTML = "X";
    grille[Math.floor(col.dataset.num / 3)][col.dataset.num % 3] = "X";
  }
  else {
    col.getElementsByTagName("p").item(0).innerHTML = "O";
    grille[Math.floor(col.dataset.num / 3)][col.dataset.num % 3] = "O";
  }

  // Verification de si la partie est terminée ou non
  let gagnant = verif_victoire();

  // Si oui, affichage de qui a gagné
  if(gagne) {
    game_message.innerHTML = `Le joueur ${gagnant} a gagné!`;
  }
  // Cas d'égalité
  else if(!grille[0].includes("-") && !grille[1].includes("-") && !grille[2].includes("-")) {
    game_message.innerHTML = "C'est une égalité!";
  }

  // Sinon, on change de joueur et on continue
  else {
    player_one = !player_one;
    if(player_one) {
      game_message.innerHTML = "C'est au premier joueur! (X)";
    }
    else {
      game_message.innerHTML = "C'est au deuxième joueur! (O)";
    }
  }
}

// Permet de vérifier si la partie est gagnée par un des joueurs ou non
function verif_victoire() {
  //Pour tester première ligne, dernière colonne et diagonale partant d'en haut à droite
  let signe_courant = grille[0][2];
  if(signe_courant !== "-" && ((signe_courant === grille[0][1] && signe_courant === grille[0][0])
      || (signe_courant === grille[1][2] && signe_courant === grille[2][2])
      || (signe_courant === grille [1][1] && signe_courant === grille[2][0])))
  {
    gagne = true;
    return player_one ? 1 : 2;
  }

  // Verification croix partant du centre et deuxième diagonale
  signe_courant = grille[1][1];
  if(signe_courant !== "-" && ((signe_courant === grille[0][1] && signe_courant === grille[2][1])
      || (signe_courant === grille[1][0] && signe_courant === grille[1][2])
      || (signe_courant === grille [0][0] && signe_courant === grille[2][2])))
  {
    gagne = true;
    return player_one ? 1 : 2;
  }

  // Verification premiere colonne et dernière ligne
  signe_courant = grille[2][0];
  if(signe_courant !== "-" && ((signe_courant === grille[2][1] && signe_courant === grille[2][2])
      || (signe_courant === grille[1][0] && signe_courant === grille[0][0])))
  {
    gagne = true;
    return player_one ? 1 : 2;
  }
}
