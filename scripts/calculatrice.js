var buttons = document.querySelectorAll("button");
var textarea = document.getElementById("screen");
var nombre1 = -1;
var nombre2 = -1;
var operateur = "";

buttons.forEach(b => {
  b.addEventListener("click", function() {
    calculatrice(this.value);
  });
});

function calculatrice(value) {
  // Cas où on entre un nombre
  if(!isNaN(value)) {
    // Si pas d'opérateur, on ajoute au premier nombre
    if(operateur === "") {
      if(nombre1 <= 0) {
        nombre1 = value;
      }
      else {
        nombre1 += value;
      }
    }
    // Si opérateur, on ajoute au deuxième
    else {
      if(nombre2 <= 0) {
        nombre2 = value;
      }
      else {
        nombre2 += value;
      }
    }
  }
  // Cas ou on entre un opérateur
  else if (value != "result" && nombre1 >= 0) {
    operateur = value;
  }

  // Affichage dans l'écran
  let res = "";
  if(nombre1 >= 0) {
    res += nombre1;
  }
  if(operateur != "") {
    res += " " + operateur;
  }
  if(nombre2 >= 0) {
    res += " " + nombre2;
  }
  // Si on a cliqué sur Calculer
  if(value == "result" && nombre1 >= 0 && nombre2 >= 0 && operateur != "") {
    let calcul = 0;
    switch(operateur) {
      case "+":
        calcul = nombre1 + nombre2;
        break;
      case "-":
        calcul = nombre1 - nombre2;
        break;
      case "x":
        calcul = nombre1 * nombre2;
        break;
      case "/":
        if(nombre2 != 0) {
          calcul = nombre1 / nombre2;
        }
        // Division par 0
        else {
          res += "\n\nImpossible de diviser par 0";
          // RETOUR EN CAS DERREUR
          textarea.value = res;
          return;
        }
        break;
      default:
        break;
    }
    res += "\n\n"+calcul;
    nombre1 = -1;
    nombre2 = -1;
    operateur = "";
  }

  textarea.value = res;
}
